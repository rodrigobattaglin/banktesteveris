package com.example.banktest.dagger

import android.app.Application
import android.content.Context
import com.example.banktest.data.DataHelper
import com.example.banktest.data.DataHelperImpl
import com.example.banktest.data.api.ApiClient
import com.example.banktest.data.api.ApiClientImpl
import com.example.banktest.data.preferences.PreferencesHelper
import com.example.banktest.data.preferences.PreferencesHelperImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideApplication(application: Application): Context {
        // Config.register(TinkConfig.LATEST)
        return application.applicationContext
    }

    @Provides
    @Singleton
    internal fun provideApi(): ApiClient {
        return ApiClientImpl()
    }

    @Provides
    @Singleton
    internal fun provideDataHelper(api: ApiClient, preferences: PreferencesHelper): DataHelper {
        return DataHelperImpl(api, preferences)
    }

    @Provides
    @Singleton
    internal fun providePreferencesHelper(context: Context): PreferencesHelper {
        return PreferencesHelperImpl(context)
    }
}
