package com.example.banktest.dagger

import com.example.banktest.ui.login.LoginActivity
import com.example.banktest.ui.login.LoginModule
import com.example.banktest.ui.statement.StatementActivity
import com.example.banktest.ui.statement.StatementModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(LoginModule::class))
    internal abstract fun loginActivity(): LoginActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(StatementModule::class))
    internal abstract fun statementActivity(): StatementActivity
}