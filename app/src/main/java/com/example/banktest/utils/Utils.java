package com.example.banktest.utils;

import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utils {

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static String formatBalance(float balance) {
        boolean positive = false;
        if(balance >= 0) positive = true;
        NumberFormat balanceFormat = NumberFormat.getCurrencyInstance();
        String balString = balanceFormat.format(balance);
        if (positive) return balString.substring(0, 2) + balString.substring(3);
        else return balString.substring(0, 3) + balString.substring(4);
    }

    public static String formatAccount(String bankAccount, String agency) {
        if (bankAccount.length() != 4 || agency.length() != 9) return "erro";
        return bankAccount + " / " +
                agency.substring(0, 2) + "." +
                agency.substring(2, 8) + "-" +
                agency.substring(8);
    }

    public static String formatDate(String date) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LocalDate localDate = LocalDate.parse(date);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String formattedDate = localDate.format(formatter);
            return formattedDate;
        }
        else {
            Date nDate = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String answer = formatter.format(nDate);
            return answer;
        }
    }
}
