package com.example.banktest.utils

import android.app.Activity
import android.content.Intent
import com.example.banktest.ui.statement.StatementActivity
import com.example.banktest.data.model.LoginResponse

class RedirectIntents {

    companion object {

        const val EXTRA_LOGIN_RESPONSE : String = "EXTRA_LOGIN_RESPONSE"

        fun redirectDetail(activity: Activity, loginResponse: LoginResponse) {
            val intent = Intent(activity, StatementActivity::class.java)
            intent.putExtra(EXTRA_LOGIN_RESPONSE, loginResponse)
            activity.startActivity(intent)
        }
    }
}