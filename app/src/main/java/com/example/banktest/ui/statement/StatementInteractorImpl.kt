package com.example.banktest.ui.statement

import com.example.banktest.data.DataHelper
import com.example.banktest.data.model.StatementResponse
import com.example.banktest.ui.base.BaseInteractorImpl
import io.reactivex.Observable
import javax.inject.Inject

class StatementInteractorImpl@Inject
constructor(dataHelper: DataHelper) : BaseInteractorImpl(dataHelper), StatementInteractor {

    override fun getStatement(id: Int) : Observable<StatementResponse> {
        return apiClient().getStatement(id)
            .flatMap { values ->
                Observable.just(values)
            }
    }
}