package com.example.banktest.ui.statement

import com.example.banktest.dagger.ActivityScoped
import dagger.Binds
import dagger.Module

@Module
abstract class StatementModule {

    @ActivityScoped
    @Binds
    internal abstract fun getView(activity: StatementActivity): StatementView

    @ActivityScoped
    @Binds
    internal abstract fun getPresenter(presenter: StatementPresenterImpl<StatementView, StatementInteractor>): StatementPresenter<StatementView, StatementInteractor>

    @ActivityScoped
    @Binds
    internal abstract fun getInteractor(interactor: StatementInteractorImpl): StatementInteractor
}