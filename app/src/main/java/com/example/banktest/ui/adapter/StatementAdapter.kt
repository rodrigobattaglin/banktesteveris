package com.example.banktest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.banktest.R
import com.example.banktest.data.model.StatementResponse
import com.example.banktest.utils.Utils

class StatementAdapter : RecyclerView.Adapter<StatementAdapter.ViewHolder>() {

    interface Listener {

    }

    var mDataSet: MutableList<StatementResponse.Statement> = arrayListOf()

    fun sortStatementByDate(statement : MutableList<StatementResponse.Statement>) : MutableList<StatementResponse.Statement> {
        val sortedByDate = statement.sortedByDescending { it.date }
        return sortedByDate.toMutableList()
    }

    fun addItems(items: MutableList<StatementResponse.Statement>) {
        val sortedItems = sortStatementByDate(items)
        mDataSet.clear()
        mDataSet.addAll(sortedItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(
                parent.context
            ).inflate(R.layout.statement_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mDataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mDataSet[position]
        holder.title.setText(item.title)
        holder.desc.setText(item.desc)
        holder.date.setText(Utils.formatDate(item.date!!))
        holder.value.setText(Utils.formatBalance(item.value!!))
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<AppCompatTextView>(R.id.tvTitle)
        val desc = itemView.findViewById<AppCompatTextView>(R.id.tvDescription)
        val date = itemView.findViewById<AppCompatTextView>(R.id.tvDate)
        val value = itemView.findViewById<AppCompatTextView>(R.id.tvValue)
    }
}
