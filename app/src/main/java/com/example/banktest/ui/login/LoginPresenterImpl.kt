package com.example.banktest.ui.login

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.ui.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginPresenterImpl<V : LoginView, I : LoginInteractor>
@Inject
constructor(mView: V, mInteractor: I) : BasePresenterImpl<V, I>(mView, mInteractor), LoginPresenter<V, I> {

    override fun checkFields(user: String, password: String) : Boolean {
        return checkUser(user) && checkPassword(password)
    }

    override fun checkUser(user: String) : Boolean {
        if(
            (user.split("@").size == 2 && !user[0].equals('@') && user.contains(".com")) ||
            (user.length == 14 && user[3].equals('.') && user[7].equals('.') && user[11].equals('-'))
        ) {
            return true
        }
        return false
    }

    override fun checkPassword(password: String) : Boolean {
        var upperChar = false
        var specialChar = false
        var numberChar = false
        for (char in password) {
            if (char.isDigit()) numberChar = true
            else if (char.isUpperCase()) upperChar = true
            else if (char.isSpecialChar()) specialChar = true
        }
        return upperChar && specialChar && numberChar
    }

    fun Char.isSpecialChar() = toLowerCase() !in 'a'..'z' && !isDigit() && !isWhitespace()

    override fun sendLogin(user: String, password: String) {
        getView().addDisposable(
            getInteractor().postLogin(user, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeWith(object : DisposableObserver<LoginResponse>() {
                    override fun onNext(login: LoginResponse) {
                        if (login.error?.code == null) {
                            getView().redirectStatement(login)
                            saveUserPreference(user)
                        }
                        else getView().showToast(login.error?.message!!)
                        getView().hideLoading()
                    }

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {
                        getView().showToast("Erro ao efetuar login $e")
                        getView().hideLoading()
                    }
                })
        )
    }

    override fun sendLoginJson(request: LoginRequest) {
        getView().addDisposable(
            getInteractor().postLoginJson(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeWith(object : DisposableObserver<LoginResponse>() {
                    override fun onNext(login: LoginResponse) {
                        if (login.error?.code == null) {
                            saveUserPreference(request.user!!)
                            getView().redirectStatement(login)
                        }
                        else getView().showToast(login.error?.message!!)
                        getView().hideLoading()
                    }

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {
                        getView().showToast("Erro ao efetuar login $e")
                        getView().hideLoading()
                    }
                })
        )
    }

    fun saveUserPreference(user: String) {
        getInteractor().saveUser(user)
    }

    override fun getUserPreference() : String {
        return getInteractor().getUser()
    }

    /*val apiService = ApiClient.client.create(ApiEndpoints::class.java)

        // val call = apiService.postLogin(user, password) // Chamada por FormUrlEncoded

        val call = apiService.postLoginJson(
            LoginRequest(
                user,
                password
            )
        ) // Chamada por Json

        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                val statusCode = response.code()
                val login = response.body()
                if (statusCode == 200 && login?.error?.code == null) redirectStatement(login!!)
                else showToast(statusCode.toString() + login?.error?.message)
                hideLoading()
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                showToast("Erro ao efetuar login " + t.toString())
                hideLoading()
            }
        })*/
}