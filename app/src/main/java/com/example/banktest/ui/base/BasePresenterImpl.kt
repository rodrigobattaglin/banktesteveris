package com.example.banktest.ui.base

import javax.inject.Inject

open class BasePresenterImpl<V : BaseView, I : BaseInteractor>
@Inject
constructor(private val mView: V, private val mInteractor: I) : BasePresenter<V, I> {

    fun getView(): V {
        return mView
    }

    fun getInteractor(): I {
        return mInteractor
    }
}