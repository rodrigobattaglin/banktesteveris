package com.example.banktest.ui.login

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.ui.base.BaseInteractor
import io.reactivex.Observable

interface LoginInteractor : BaseInteractor{
    fun postLogin(user: String, password: String): Observable<LoginResponse>
    fun postLoginJson(request: LoginRequest): Observable<LoginResponse>
    fun saveUser(user: String)
    fun getUser(): String
}