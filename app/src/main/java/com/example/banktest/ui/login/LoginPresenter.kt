package com.example.banktest.ui.login

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.ui.base.BasePresenter

interface LoginPresenter<V : LoginView, I : LoginInteractor> : BasePresenter<V, I> {
    fun checkFields(user: String, password: String): Boolean
    fun sendLogin(user: String, password: String)
    fun sendLoginJson(request: LoginRequest)
    fun getUserPreference(): String
    fun checkUser(user: String): Boolean
    fun checkPassword(password: String): Boolean
}