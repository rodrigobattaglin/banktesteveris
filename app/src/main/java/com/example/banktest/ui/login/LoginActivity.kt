package com.example.banktest.ui.login

import android.os.Bundle
import android.view.View
import com.example.banktest.R
import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.ui.base.BaseViewActivity
import com.example.banktest.utils.RedirectIntents
import com.example.banktest.utils.Utils
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : BaseViewActivity(), LoginView {

    @Inject
    lateinit var mPresenter : LoginPresenter<LoginView, LoginInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener { btnLoginClick() }

        etUser.setText(mPresenter.getUserPreference())

        tvBranchName.text = getString(R.string.gitBranch)
    }

    fun btnLoginClick() {
        showLoading()
        val user = etUser.text.toString()
        val password = etPassword.text.toString()
        if (mPresenter.checkFields(user, password)) {
            // mPresenter.sendLogin(user, password)
            mPresenter.sendLoginJson(LoginRequest(user, password))
        }
        else {
            showToast("Usuário ou senha inválidas")
            hideLoading()
        }
    }

    override fun redirectStatement(loginResponse: LoginResponse) {
        RedirectIntents.redirectDetail(this, loginResponse)
    }

    override fun showToast(text: String) {
        Utils.showToast(this, text)
    }

    override fun showLoading() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
    }
}
