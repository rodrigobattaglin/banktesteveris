package com.example.banktest.ui.statement

import com.example.banktest.ui.base.BasePresenter

interface StatementPresenter<V : StatementView, I : StatementInteractor> : BasePresenter<V, I> {
    fun getStatement(id: Int)
}