package com.example.banktest.ui.login

import com.example.banktest.data.DataHelper
import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.ui.base.BaseInteractorImpl
import io.reactivex.Observable
import javax.inject.Inject

class LoginInteractorImpl
@Inject
constructor(dataHelper: DataHelper) : BaseInteractorImpl(dataHelper), LoginInteractor {

    override fun postLogin(user: String, password: String) : Observable<LoginResponse> {
        return apiClient().postLogin(user, password)
            .flatMap { values ->
                Observable.just(values)
            }
    }

    override fun postLoginJson(request: LoginRequest) : Observable<LoginResponse> {
        return apiClient().postLoginJson(request)
            .flatMap { values ->
                Observable.just(values)
            }
    }

    override fun saveUser(user: String) {
        saveUserPreference(user)
    }

    override fun getUser(): String {
        return getUserPreference()
    }
}