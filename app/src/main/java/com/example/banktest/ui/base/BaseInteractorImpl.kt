package com.example.banktest.ui.base

import com.example.banktest.data.DataHelper
import com.example.banktest.data.api.ApiClient
import com.example.banktest.data.preferences.PreferencesHelper
import javax.inject.Inject

open class BaseInteractorImpl
@Inject
constructor(private val dataHelper: DataHelper) : BaseInteractor {

    fun apiClient(): ApiClient {
        return dataHelper.api()
    }

    fun saveUserPreference(user: String) {
        dataHelper.preferences().saveUser(user)
    }

    fun getUserPreference() : String {
        return dataHelper.preferences().getUser()
    }
}
