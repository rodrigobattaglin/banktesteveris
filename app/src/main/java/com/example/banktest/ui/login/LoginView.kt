package com.example.banktest.ui.login

import com.example.banktest.data.model.LoginResponse
import com.example.banktest.ui.base.BaseView

interface LoginView : BaseView {
    fun redirectStatement(loginResponse: LoginResponse)
}