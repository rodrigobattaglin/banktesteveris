package com.example.banktest.ui.base

import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable

abstract class BaseViewActivity : DaggerAppCompatActivity(), BaseView {

    private val mBaseView = BaseViewImpl()

    override fun onDestroy() {
        mBaseView.onDestroy()
        super.onDestroy()
    }

    override fun addDisposable(disposable: Disposable) {
        mBaseView.addDisposable(disposable)
    }

    /*override fun showContentLoading() {
        mBaseView.showContentLoading(this, getContentMain(), getWidgetLoadView())
    }

    override fun hideContentLoading() {
        mBaseView.hideContentLoading(this, getContentMain(), getWidgetLoadView())
    }

    open fun getContentMain(): View? {
        return null
    }

    open fun getWidgetLoadView(): ContentLoadView? {
        return null
    }

    override fun onErrorHandler(e: Throwable, listener : ContentLoadView.OnClickListener) {
        mBaseView.onErrorHandler(this, getContentMain(), getWidgetLoadView(), e, listener)
    }*/
}