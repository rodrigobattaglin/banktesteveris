package com.example.banktest.ui.statement

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.banktest.R
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.data.model.StatementResponse
import com.example.banktest.ui.adapter.StatementAdapter
import com.example.banktest.ui.base.BaseViewActivity
import com.example.banktest.utils.RedirectIntents
import com.example.banktest.utils.Utils
import kotlinx.android.synthetic.main.statement_content.*
import kotlinx.android.synthetic.main.statement_header.*
import javax.inject.Inject

class StatementActivity : BaseViewActivity(), StatementView, StatementAdapter.Listener {

    @Inject
    lateinit var mPresenter : StatementPresenter<StatementView, StatementInteractor>

    lateinit var loginResponse : LoginResponse

    lateinit var mAdapter: StatementAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statement)

        loginResponse = intent.getParcelableExtra(RedirectIntents.EXTRA_LOGIN_RESPONSE)

        val account = Utils.formatAccount(loginResponse.userAccount?.bankAccount!!, loginResponse.userAccount?.agency!!)
        val balance = Utils.formatBalance(loginResponse.userAccount?.balance!!)

        tvNome.text = loginResponse.userAccount?.name
        tvNumeroConta.text = account
        tvNumeroSaldo.text = balance

        ivExit.setOnClickListener { onBackPressed() }

        mAdapter = StatementAdapter()
        // mAdapter.setHasStableIds(true)
        rcStatement.adapter = mAdapter
        rcStatement.layoutManager = LinearLayoutManager(this@StatementActivity, RecyclerView.VERTICAL, false)
        rcStatement.setHasFixedSize(true)
        // rcStatement.setItemViewCacheSize(10)

        mPresenter.getStatement(loginResponse.userAccount?.userId?.toInt()!!)
    }

    override fun showToast(text: String) {
        Utils.showToast(this, text)
    }

    override fun addItems(items: MutableList<StatementResponse.Statement>) {
        mAdapter.addItems(items)
    }

    override fun showLoading() {
        rcStatement.visibility = View.GONE
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
        rcStatement.visibility = View.VISIBLE
    }

    /*fun getStatement(id: Int) {
        val apiService = ApiClient.client.create(ApiEndpoints::class.java)

        val call = apiService.getStatement(id) // Chamada por FormUrlEncoded

        call.enqueue(object : Callback<StatementResponse> {
            override fun onResponse(call: Call<StatementResponse>, response: Response<StatementResponse>) {
                val statusCode = response.code()
                val statement = response.body()
                if (statusCode == 200) addItems(statement?.statementList!!)
                else showToast("Erro ao carregar histórico")
                hideLoading()
            }

            override fun onFailure(call: Call<StatementResponse>, t: Throwable) {
                showToast("Falha ao buscar histórico " + t.toString())
                hideLoading()
            }
        })
    }*/
}
