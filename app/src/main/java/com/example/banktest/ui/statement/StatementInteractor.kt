package com.example.banktest.ui.statement

import com.example.banktest.data.model.StatementResponse
import com.example.banktest.ui.base.BaseInteractor
import io.reactivex.Observable

interface StatementInteractor : BaseInteractor {
    fun getStatement(id: Int): Observable<StatementResponse>
}