package com.example.banktest.ui.statement

import com.example.banktest.data.model.StatementResponse
import com.example.banktest.ui.base.BaseView

interface StatementView : BaseView {
    fun addItems(items: MutableList<StatementResponse.Statement>)
}