package com.example.banktest.ui.base

import android.view.View
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.Disposable

abstract class BaseViewFragment : DaggerFragment(), BaseView {

    private val mBaseView = BaseViewImpl()

    override fun onDestroy() {
        mBaseView.onDestroy()
        super.onDestroy()
    }

    override fun addDisposable(disposable: Disposable) {
        mBaseView.addDisposable(disposable)
    }

    /*override fun showContentLoading() {
        mBaseView.showContentLoading(activity!!, getContentMain(), getWidgetLoadView())
    }

    override fun hideContentLoading() {
        mBaseView.hideContentLoading(activity!!, getContentMain(), getWidgetLoadView())
    }

    fun getWidgetLoadView(): ContentLoadView? {
        return null
    }*/

    fun getContentMain(): View? {
        return null
    }

    /*override fun onErrorHandler(e: Throwable, listener : ContentLoadView.OnClickListener) {
        mBaseView.onErrorHandler(activity!!, getContentMain(), getWidgetLoadView(), e, listener)
    }*/
}