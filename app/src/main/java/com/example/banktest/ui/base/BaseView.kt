package com.example.banktest.ui.base

import io.reactivex.disposables.Disposable

interface BaseView {
    fun addDisposable(disposable: Disposable)
    fun showToast(text: String)
    fun hideLoading()
    fun showLoading()
}