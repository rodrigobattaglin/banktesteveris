package com.example.banktest.ui.statement

import com.example.banktest.data.model.StatementResponse
import com.example.banktest.ui.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StatementPresenterImpl<V : StatementView, I : StatementInteractor>
@Inject
constructor(mView: V, mInteractor: I) : BasePresenterImpl<V, I>(mView, mInteractor), StatementPresenter<V, I> {

    override fun getStatement(id: Int) {
        getView().addDisposable(
            getInteractor().getStatement(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeWith(object : DisposableObserver<StatementResponse>() {
                    override fun onNext(statement: StatementResponse) {
                        getView().addItems(statement?.statementList!!)
                        getView().hideLoading()
                    }

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {
                        getView().showToast("Erro ao carregar histórico")
                        getView().hideLoading()
                    }
                })
        )
    }
}