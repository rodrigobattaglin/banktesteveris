package com.example.banktest

import com.example.banktest.dagger.DaggerAppComponent
import com.example.banktest.data.DataHelper
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class AppApplication : DaggerApplication() {

    /*override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }*/

    @Inject
    lateinit var datahelper: DataHelper

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}