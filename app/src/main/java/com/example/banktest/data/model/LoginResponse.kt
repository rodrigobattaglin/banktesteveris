package com.example.banktest.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class LoginResponse : Parcelable {

    @SerializedName("userAccount")
    var userAccount: UserAccount? = null

    @SerializedName("error")
    var error: Erro? = null

    constructor()

    constructor(nUserAccount: UserAccount, nError: Erro) {
        userAccount = nUserAccount
        error = nError
    }

    constructor(parcel: Parcel) {
        userAccount = parcel.readValue(UserAccount.javaClass.classLoader) as UserAccount?
        error = parcel.readValue(Erro.javaClass.classLoader) as Erro?
    }

    override fun describeContents() = 0

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(userAccount)
        parcel.writeValue(error)
    }

    companion object CREATOR : Parcelable.Creator<LoginResponse> {
        override fun createFromParcel(parcel: Parcel): LoginResponse {
            return LoginResponse(parcel)
        }

        override fun newArray(size: Int): Array<LoginResponse?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return "[UserAccount: " + userAccount + ", Error: " + error + "]"
    }

    class UserAccount : Parcelable {

        @SerializedName("userId")
        var userId: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("bankAccount")
        var bankAccount: String? = null

        @SerializedName("agency")
        var agency: String? = null

        @SerializedName("balance")
        var balance: Float? = null

        constructor()

        constructor(userId: String?, name: String?, bankAccount: String?, agency: String?, balance: Float?) {
            this.userId = userId
            this.name = name
            this.bankAccount = bankAccount
            this.agency = agency
            this.balance = balance
        }

        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat()
        )

        override fun describeContents() = 0

        override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
            parcel.writeString(userId)
            parcel.writeString(name)
            parcel.writeString(bankAccount)
            parcel.writeString(agency)
            parcel.writeFloat(balance!!)
        }

        companion object CREATOR : Parcelable.Creator<UserAccount> {
            override fun createFromParcel(parcel: Parcel): UserAccount {
                return UserAccount(parcel)
            }

            override fun newArray(size: Int): Array<UserAccount?> {
                return arrayOfNulls(size)
            }
        }

        override fun toString(): String {
            return "[UserId: " + userId + ", Name: " + name + ", BankAccount: " + bankAccount +
                    ", Agency: " + agency + ", Balance: " + balance + "]"
        }
    }

    class Erro : Parcelable {

        @SerializedName("code")
        var code: String? = null

        @SerializedName("message")
        var message: String? = null

        constructor()

        constructor(code: String?, message: String?) {
            this.code = code
            this.message = message
        }

        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
            parcel.writeString(code)
            parcel.writeString(message)
        }

        companion object CREATOR : Parcelable.Creator<Erro> {
            override fun createFromParcel(parcel: Parcel): Erro {
                return Erro(parcel)
            }

            override fun newArray(size: Int): Array<Erro?> {
                return arrayOfNulls(size)
            }
        }

        override fun toString(): String {
            return "[Code: " + code + ", Message: " + message + "]"
        }
    }
}