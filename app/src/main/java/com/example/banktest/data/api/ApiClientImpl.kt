package com.example.banktest.data.api

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.data.model.StatementResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiClientImpl : ApiClient {

    val BASE_URL = "https://bank-app-test.herokuapp.com/api/"
    // private var retrofit: Retrofit? = null

    val mApiEndpoints : ApiEndpoints

    init {
        /*val loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.d("CLIENT-API", message) })
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()
         */
        mApiEndpoints = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            // .client(httpClient)
            .build()
            .create(ApiEndpoints::class.java)
    }

    /*val client: Retrofit
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }*/

    override fun postLogin(user: String, password: String) : Observable<LoginResponse> {
        return mApiEndpoints.postLogin(user, password)
    }

    override fun postLoginJson(request: LoginRequest) : Observable<LoginResponse> {
        return mApiEndpoints.postLoginJson(request)
    }

    override fun getStatement(id: Int) : Observable<StatementResponse> {
        return mApiEndpoints.getStatement(id)
    }
}
