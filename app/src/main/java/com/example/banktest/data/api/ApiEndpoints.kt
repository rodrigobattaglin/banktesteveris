package com.example.banktest.data.api

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.data.model.StatementResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*


interface ApiEndpoints {

    @FormUrlEncoded
    @POST("login")
    fun postLogin(@Field("user") user: String, @Field("password") password: String): Observable<LoginResponse>

    @POST("login")
    fun postLoginJson(@Body login: LoginRequest): Observable<LoginResponse>

    @GET("statements/{id}")
    fun getStatement(@Path("id") id: Int): Observable<StatementResponse>

    /*@FormUrlEncoded
    @POST("login")
    fun postLogin(@Field("user") user: String, @Field("password") password: String): Call<LoginResponse>

    @POST("login")
    fun postLoginJson(@Body login: LoginRequest): Call<LoginResponse>

    @GET("statements/{id}")
    fun getStatement(@Path("id") id: Int): Call<StatementResponse>*/
}