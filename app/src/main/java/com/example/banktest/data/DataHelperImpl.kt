package com.example.banktest.data

import com.example.banktest.data.api.ApiClient
import com.example.banktest.data.preferences.PreferencesHelper

class DataHelperImpl(private var apiClient: ApiClient,
                     private var mPreferences: PreferencesHelper) : DataHelper {

    override fun api(): ApiClient {
        return apiClient
    }

    override fun preferences() : PreferencesHelper {
        return mPreferences
    }
}