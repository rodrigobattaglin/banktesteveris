package com.example.banktest.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class StatementResponse : Parcelable {

    @SerializedName("statementList")
    var statementList: MutableList<Statement>? = null

    constructor(statementList: MutableList<Statement>?) {
        this.statementList = statementList
    }

    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Statement)
    )

    override fun describeContents() = 0

    override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
        parcel.writeTypedList(statementList)
    }

    companion object CREATOR : Parcelable.Creator<Statement> {
        override fun createFromParcel(parcel: Parcel): Statement {
            return Statement(parcel)
        }

        override fun newArray(size: Int): Array<Statement?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return "[StatementList: " + statementList + "]"
    }

    class Statement : Parcelable {
        @SerializedName("title")
        var title: String? = null

        @SerializedName("desc")
        var desc: String? = null

        @SerializedName("date")
        var date: String? = null

        @SerializedName("value")
        var value: Float? = null

        constructor(nTitle: String?, nDesc: String?, nDate: String?, nValue: Float?) {
            title = nTitle
            desc = nDesc
            date = nDate
            value = nValue
        }

        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat()
        )

        override fun describeContents() = 0

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(title)
            parcel.writeString(desc)
            parcel.writeString(date)
            parcel.writeFloat(value!!)
        }

        companion object CREATOR : Parcelable.Creator<Statement> {
            override fun createFromParcel(parcel: Parcel): Statement {
                return Statement(parcel)
            }

            override fun newArray(size: Int): Array<Statement?> {
                return arrayOfNulls(size)
            }
        }

        override fun toString(): String {
            return "[Title: " + title + ", Desc: " + desc + ", Date: " + date + ", Value: " + value + "]"
        }
    }
}