package com.example.banktest.data.api

import com.example.banktest.data.model.LoginRequest
import com.example.banktest.data.model.LoginResponse
import com.example.banktest.data.model.StatementResponse
import io.reactivex.Observable

interface ApiClient {
    fun postLogin(user: String, password: String): Observable<LoginResponse>
    fun postLoginJson(request: LoginRequest) : Observable<LoginResponse>
    fun getStatement(id: Int) : Observable<StatementResponse>
}
