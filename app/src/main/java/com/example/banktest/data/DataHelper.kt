package com.example.banktest.data

import com.example.banktest.data.api.ApiClient
import com.example.banktest.data.preferences.PreferencesHelper

interface DataHelper {
    fun api(): ApiClient
    fun preferences(): PreferencesHelper
}