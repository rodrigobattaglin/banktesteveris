package com.example.banktest.data.preferences

import android.content.Context
import android.content.SharedPreferences

class PreferencesHelperImpl(context: Context) : PreferencesHelper {

    private val PREFS_NAME = "bank_preferences"
    private val PREF_USER = "PREF_USER"

    private val mPreferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    override fun getUser() : String {
        return mPreferences.getString(PREF_USER, "")!!
    }

    override fun saveUser(user: String) {
        val editor = mPreferences.edit()
        editor.putString(PREF_USER, user)
        editor.apply()
        editor.commit()
    }

}
