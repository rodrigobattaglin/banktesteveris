package com.example.banktest.data.preferences

interface PreferencesHelper {

    fun getUser(): String
    fun saveUser(user: String)
}
