package com.example.banktest.data.model

class LoginRequest {

    var user : String? = null

    var password : String? = null

    constructor(user: String?, password: String?) {
        this.user = user
        this.password = password
    }
}