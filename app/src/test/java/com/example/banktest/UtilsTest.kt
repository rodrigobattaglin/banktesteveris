package com.example.banktest

import com.example.banktest.utils.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class UtilsTest {

    @Before
    fun setUp() {

    }

    @Test
    fun formatAccount_isCorrect() {
        var bankAccount = ""
        var agency = ""

        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "123"
        agency = "012345678"
        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "12345"
        agency = "012345678"
        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "1234"
        agency = "01234567"
        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "1234"
        agency = "0123456789"
        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "12345"
        agency = "0123456789"
        Assert.assertEquals("erro", Utils.formatAccount(bankAccount, agency))

        bankAccount = "1234"
        agency = "012345678"
        Assert.assertEquals("1234 / 01.234567-8", Utils.formatAccount(bankAccount, agency))

        bankAccount = "5678"
        agency = "123456789"
        Assert.assertEquals("5678 / 12.345678-9", Utils.formatAccount(bankAccount, agency))
    }

    @Test
    fun formatBalance_isCorrect() {
        var balance = 0.0f

        Assert.assertEquals("R$0,00", Utils.formatBalance(balance))

        balance = 10.0f
        Assert.assertEquals("R$10,00", Utils.formatBalance(balance))

        balance = 100.0f
        Assert.assertEquals("R$100,00", Utils.formatBalance(balance))

        balance = 1000.5f
        Assert.assertEquals("R$1.000,50", Utils.formatBalance(balance))

        balance = 500.23f
        Assert.assertEquals("R$500,23", Utils.formatBalance(balance))

        balance = 3.2341f
        Assert.assertEquals("R$3,23", Utils.formatBalance(balance))

        balance = 3.2351f
        Assert.assertEquals("R$3,24", Utils.formatBalance(balance))

        balance = 3.3445f
        Assert.assertEquals("R$3,34", Utils.formatBalance(balance))

        balance = 1005000.0f
        Assert.assertEquals("R$1.005.000,00", Utils.formatBalance(balance))
    }
}