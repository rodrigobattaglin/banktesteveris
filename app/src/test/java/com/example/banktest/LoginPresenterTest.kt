package com.example.banktest

import com.example.banktest.ui.login.LoginInteractor
import com.example.banktest.ui.login.LoginPresenter
import com.example.banktest.ui.login.LoginPresenterImpl
import com.example.banktest.ui.login.LoginView
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LoginPresenterTest {

    @Mock
    lateinit var mView : LoginView

    @Mock
    lateinit var mInteractor : LoginInteractor

    private lateinit var mPresenter : LoginPresenter<LoginView, LoginInteractor>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = LoginPresenterImpl(mView, mInteractor)
    }

    @Test
    fun checkUser_isCorrect() {
        var user = ""

        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "teste"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "@teste"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = ".com"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "@everis.com"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "te@ste@everis.com"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "teste@everis.com"
        Assert.assertEquals(true, mPresenter.checkUser(user))

        user = "tes@everis.com"
        Assert.assertEquals(true, mPresenter.checkUser(user))

        user = "12345678901"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "12334566789901"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "123.4566789901"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "123.456.789901"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "123.456-789-01"
        Assert.assertEquals(false, mPresenter.checkUser(user))

        user = "123.456.789-01"
        Assert.assertEquals(true, mPresenter.checkUser(user))
    }

    @Test
    fun checkPassword_isCorrect() {
        var password = ""

        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "teste"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "Teste"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "1231"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "!@#"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "Teste1"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "Teste!"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "123$!@"
        Assert.assertEquals(false, mPresenter.checkPassword(password))

        password = "Teste123!"
        Assert.assertEquals(true, mPresenter.checkPassword(password))
    }
}